# Mass balance calculation preprocessing module
import numpy as np
from netCDF4 import Dataset
import os
from . import WQMass, WQOutput


class GlobalParameters:
    def __init__(self):
        self.units = 1e6
        self.secs_per_day = 86400
        self.secs_per_hour = 3600
        self.L_per_m3 = 1000
        self.ug_per_mg = 1000
        self.Xon = 2.0
        self.C_mm_mg = 12
        self.N_mm_mg = 14
        self.P_mm_mg = 31
        self.O2_mm_mg = 32
        self.Si_mm_mg = 28
        self.sim_class = 0
        self.dt = 0
        self.anammox_amm = 1.0
        self.anammox_nit = 1.32
        self.conv_volu = self.dt * self.L_per_m3 / self.units / self.secs_per_day
        self.conv_volu_CFU = 10 * self.dt * self.L_per_m3 / 1 / self.secs_per_day
        self.conv_area = self.dt * 1 / self.units / self.secs_per_day
        self.conv_area_CFU = self.dt * 1 / 1 / self.secs_per_day
        self.conv_mass = self.L_per_m3 / self.units
        self.conv_mass_CFU = 10 * self.L_per_m3 / 1
        self.lw = 2
        self.dy = 0.05
        self.startdate = []
        self.tol = 0.1
        self.save_all = 1,
        self.X_cc = [26.8, 27.8],
        self.X_ncon = [0.0, 3.0],
        self.X_pcon = [0.0, 0.6],
        self.X_scon = [8.0, 8.0],
        self.amm_nit_atm_split = 0.5,
        self.k_fres = [0.2, 0.3],
        self.K_fdom = [0.3, 0.4],
        self.f_pr = [0.1, 0.5],
        self.X_nrpom = 0.1,
        self.X_prpom = 0.01,
        self.f_pho = 0.5,
        self.fig_count = 1


class WQVariables:
    def __init__(self, ncell, nstep, numphy, numpth, simclass):
        zero2 = np.zeros([ncell, nstep], dtype=np.float32)
        zero3 = np.zeros([ncell, nstep, numphy], dtype=np.float32)
        self.oxy = zero2
        self.sil = zero2
        self.amm = zero2
        self.frp = zero2
        self.fra = zero2
        self.pth_ali = np.zeros([ncell, nstep, numpth], dtype=np.float32)
        self.pth_dea = np.zeros([ncell, nstep, numpth], dtype=np.float32)
        self.pth_att = np.zeros([ncell, nstep, numpth], dtype=np.float32)
        if simclass > 0:
            self.phy = zero3

    def adding_new_attr(self, attr, val):
        setattr(self, attr, val)


class WQDiagnostics:
    def __init__(self, ncell, nstep, numphy, numpth, simclass):
        zero2 = np.zeros([ncell, nstep], dtype=np.float32)
        zero3 = np.zeros([ncell, nstep, numphy], dtype=np.float32)
        zero3_pth = np.zeros([ncell, nstep, numpth], dtype=np.float32)
        self.oxy_atm = zero2
        self.oxy_sed = zero2
        self.oxy_nit = zero3
        self.oxy_gpp = zero3
        self.oxy_rsf = zero3
        self.oxy_omi = zero3
        self.pth_tot = np.zeros([ncell, nstep, numpth], dtype=np.float32)
        self.pth_grw = np.zeros([ncell, nstep, numpth], dtype=np.float32)
        self.pth_lgt_al = np.zeros([ncell, nstep, numpth], dtype=np.float32)
        self.pth_mor_al = np.zeros([ncell, nstep, numpth], dtype=np.float32)
        self.pth_lgt_de = np.zeros([ncell, nstep, numpth], dtype=np.float32)
        self.pth_mor_de = np.zeros([ncell, nstep, numpth], dtype=np.float32)
        self.pth_als = np.zeros([ncell, nstep, numpth], dtype=np.float32)
        self.pth_des = np.zeros([ncell, nstep, numpth], dtype=np.float32)
        self.pth_ats = np.zeros([ncell, nstep, numpth], dtype=np.float32)
        self.pth_atm = np.zeros([ncell, nstep, numpth], dtype=np.float32)
        if simclass > 0:
            # self.phy_sed = zero3
            self.car_gpp = zero3
            self.car_rsf = zero3
            self.car_exc = zero3
            self.car_mor = zero3
            self.sil_upt = zero3
            self.sil_exc = zero3
            self.sil_mor = zero3
            self.amm_exc = zero3
            self.amm_mor = zero3
            self.amm_pho = zero3
            self.frp_exc = zero3
            self.frp_mor = zero3
            self.frp_pho = zero3
            self.nit_omi = zero3
            self.amm_omi = zero3
            self.frp_omi = zero3
            self.dic_pho = zero3

    def adding_new_attr(self, attr, val):
        setattr(self, attr, val)


def loadglobal(startdate,
               tol=0.1,
               save_all=1,
               X_cc=[26.8, 27.8],
               X_ncon=[0.0, 3.0],
               X_pcon=[0.0, 0.6],
               X_scon=[8.0, 8.0],
               amm_nit_atm_split=0.5,
               k_fres=[0.2, 0.3],
               K_fdom=[0.3, 0.4],
               f_pr=[0.1, 0.5],
               X_nrpom=0.1,
               X_prpom=0.01,
               f_pho=0.5,
               fig_count=1,
               ):
    globalparam = GlobalParameters()
    globalparam.startdate = startdate
    globalparam.tol = tol
    globalparam.save_all = save_all
    globalparam.X_cc = X_cc
    globalparam.X_ncon = X_ncon
    globalparam.X_pcon = X_pcon
    globalparam.X_scon = X_scon
    globalparam.amm_nit_atm_split = amm_nit_atm_split
    globalparam.k_fres = k_fres
    globalparam.k_fdom = K_fdom
    globalparam.f_pr = f_pr
    globalparam.X_nprom = X_nrpom
    globalparam.X_prpom = X_prpom
    globalparam.f_pho = f_pho
    globalparam.fig_count = fig_count
    return globalparam


def read_netcdf(out_dir, id, suffix):
    inFile = os.path.join(out_dir, id + suffix)
    nc_in = Dataset(inFile, 'r')
    return nc_in


def read_geometry(nc_in):
    area = nc_in['cell_A'][:]
    NL = nc_in['NL'][:]
    zfaces = nc_in['layerface_Z'][:]
    return area, NL, zfaces


def read_field_names(ncnames):
    phc_sed_names = []
    phn_sed_names = []
    php_sed_names = []
    phy_gpp_names = []
    phy_res_names = []
    phy_exc_names = []
    phy_exn_names = []
    phy_exp_names = []
    phy_moc_names = []
    phy_mon_names = []
    phy_mop_names = []
    for var in ncnames:
        if var.__contains__('WQ_DIAG_PHYTO') and not var.__contains__('_COM_'):
            if var.__contains__('SEDMTN_MICG_L_D'):
                phc_sed_names.append(var)
            if var.__contains__('SEDMTN_N_MG_L_D'):
                phn_sed_names.append(var)
            if var.__contains__('SEDMTN_P_MG_L_D'):
                php_sed_names.append(var)
            if var.__contains__('_PRIM_PROD_MICG_L_D'):
                phy_gpp_names.append(var)
            if var.__contains__('_RESP_MICG_L_D'):
                phy_res_names.append(var)
            if var.__contains__('_EXCR_MICG_L_D'):
                phy_exc_names.append(var)
            if var.__contains__('_EXCR_N_MG_L_D'):
                phy_exn_names.append(var)
            if var.__contains__('_EXCR_P_MG_L_D'):
                phy_exp_names.append(var)
            if var.__contains__('_MORT_MICG_L_D'):
                phy_moc_names.append(var)
            if var.__contains__('_MORT_N_MG_L_D'):
                phy_mon_names.append(var)
            if var.__contains__('_MORT_P_MG_L_D'):
                phy_mop_names.append(var)
    return phc_sed_names, phn_sed_names, php_sed_names, phy_gpp_names, phy_res_names, phy_exc_names, phy_exn_names, phy_exp_names, phy_moc_names, phy_mon_names, phy_mop_names


def read_field_names_MMM(ncnames):
    phc_sed_names = []
    phn_sed_names = []
    php_sed_names = []
    phy_gpp_names = []
    phy_res_names = []
    phy_exc_names = []
    phy_exn_names = []
    phy_exp_names = []
    phy_moc_names = []
    phy_mon_names = []
    phy_mop_names = []
    for var in ncnames:
        if var.__contains__('WQ_DIAG_PHYTO') and not var.__contains__('_COM_'):
            if var.__contains__('SEDMTN_MM_M3_D'):
                phc_sed_names.append(var)
            if var.__contains__('SEDMTN_N_MM_M3_D'):
                phn_sed_names.append(var)
            if var.__contains__('SEDMTN_P_MM_M3_D'):
                php_sed_names.append(var)
            if var.__contains__('_PRIM_PROD_MM_M3_D'):
                phy_gpp_names.append(var)
            if var.__contains__('_RESP_MM_M3_D'):
                phy_res_names.append(var)
            if var.__contains__('_EXCR_MM_M3_D'):
                phy_exc_names.append(var)
            if var.__contains__('_EXCR_N_MM_M3_D'):
                phy_exn_names.append(var)
            if var.__contains__('_EXCR_P_MM_M3_D'):
                phy_exp_names.append(var)
            if var.__contains__('_MORT_MM_M3_D'):
                phy_moc_names.append(var)
            if var.__contains__('_MORT_N_MM_M3_D'):
                phy_mon_names.append(var)
            if var.__contains__('_MORT_P_MM_M3_D'):
                phy_mop_names.append(var)
    return phc_sed_names, phn_sed_names, php_sed_names, phy_gpp_names, phy_res_names, phy_exc_names, phy_exn_names, phy_exp_names, phy_moc_names, phy_mon_names, phy_mop_names

def read_pat_field_names(ncnames):
    pat_tot_names = []
    pat_grw_names = []
    pat_lgt_names = []
    pat_mor_names = []
    pat_als_names = []
    pat_des_names = []
    pat_ats_names = []
    pat_atm_names = []
    for var in ncnames:
        if var.__contains__('WQ_DIAG_PATH'):
            if var.__contains__('_TOTAL_CFU_100ML'):
                pat_tot_names.append(var)
            if var.__contains__('_GROWTH_CFU_100ML_D'):
                pat_grw_names.append(var)
            if var.__contains__('_LIGHT_CFU_100ML_D'):
                pat_lgt_names.append(var)
            if var.__contains__('_MORTALITY_CFU_100ML_D'):
                pat_mor_names.append(var)
            if var.__contains__('_ALIVE_SEDMTN_CFU_100ML_D'):
                pat_als_names.append(var)
            if var.__contains__('_DEAD_SEDMTN_CFU_100ML_D'):
                pat_des_names.append(var)
            if var.__contains__('_ATTCHD_SEDMTN_CFU_100ML_D'):
                pat_ats_names.append(var)
            if var.__contains__('_ATTACHMENT_CFU_100ML_D'):
                pat_atm_names.append(var)
    return pat_tot_names, pat_grw_names, pat_lgt_names, pat_mor_names, pat_als_names, pat_des_names, pat_ats_names, pat_atm_names

def read_pat_field_names_mmm(ncnames):
    pat_tot_names = []
    pat_grw_names = []
    pat_lgt_names = []
    pat_mor_names = []
    pat_als_names = []
    pat_des_names = []
    pat_ats_names = []
    pat_atm_names = []
    for var in ncnames:
        if var.__contains__('WQ_DIAG_PATH'):
            if var.__contains__('_TOTAL_CFU_M3'):
                pat_tot_names.append(var)
            if var.__contains__('_GROWTH_CFU_M3_D'):
                pat_grw_names.append(var)
            if var.__contains__('_LIGHT_CFU_M3_D'):
                pat_lgt_names.append(var)
            if var.__contains__('_MORTALITY_CFU_M3_D'):
                pat_mor_names.append(var)
            if var.__contains__('_ALIVE_SEDMTN_CFU_M3_D'):
                pat_als_names.append(var)
            if var.__contains__('_DEAD_SEDMTN_CFU_M3_D'):
                pat_des_names.append(var)
            if var.__contains__('_ATTCHD_SEDMTN_CFU_M3_D'):
                pat_ats_names.append(var)
            if var.__contains__('_ATTACHMENT_CFU_M3_D'):
                pat_atm_names.append(var)
    return pat_tot_names, pat_grw_names, pat_lgt_names, pat_mor_names, pat_als_names, pat_des_names, pat_ats_names, pat_atm_names

def load_parameters(nc_in, id, globalparam):
    """Load global parameters"""
    t = nc_in['ResTime'][:]
    dt = (t[1] - t[0]) * globalparam.secs_per_hour
    tmp = id.split('_')
    for string in tmp:
        if string.startswith('d') or string.startswith('D'):
            simclass = 0
            break
        elif string.startswith('i') or string.startswith('I'):
            simclass = 1
            break
        elif string.startswith('o') or string.startswith('O'):
            simclass = 3
            break
        else:
            simclass = -1
    print(id)
    globalparam.sim_class = int(simclass)
    globalparam.dt = dt
    globalparam.conv_area = globalparam.dt * 1.000000 / globalparam.units / globalparam.secs_per_day
    globalparam.conv_volu = globalparam.dt * globalparam.L_per_m3 / globalparam.units / globalparam.secs_per_day
    globalparam.conv_volu_CFU = 10.0 * globalparam.dt * globalparam.L_per_m3 / 1.000000 / globalparam.secs_per_day
    globalparam.conv_area_CFU = globalparam.dt * 1.000000 / 1.000000 / globalparam.secs_per_day

def construct_array(nc_in, simclass):
    nc_names = list(nc_in.variables.keys())

    # pathogens
    pth_names_alive = []
    pth_names_dead = []
    pth_names_attch = []
    for var in nc_names:
        if var.__contains__('WQ_PATH_') and var.__contains__('_ALIVE_CFU_100ML'):
            pth_names_alive.append(var)
        if var.__contains__('WQ_PATH_') and var.__contains__('_DEAD_CFU_100ML'):
            pth_names_dead.append(var)
    num_pth = np.size(pth_names_alive)
    pth_model = np.zeros([num_pth, 1], dtype=np.int32)
    for i in range(0, num_pth):
        pth_name_att = pth_names_alive[i].replace('_ALIVE_', '_ATTCHD_')
        if pth_name_att in nc_names:
            pth_model[i] = 1
            pth_names_attch.append(pth_name_att)
        else:
            pth_names_attch.append('')

    # phytoplankton
    phy_names = []
    for var in nc_names:
        if var.__contains__('WQ_PHYTO_') and var.__contains__('_CONC_MICG_L'):
            phy_names.append(var)
    num_phy = np.size(phy_names)
    phy_model = np.zeros([num_phy, 1], dtype=np.int32)

    if simclass > 0:
        for i in range(0, num_phy):
            phy_name_N = phy_names[i].replace('_CONC_MICG_L', '_INT_N_MG_L')
            phy_name_P = phy_names[i].replace('_CONC_MICG_L', '_INT_P_MG_L')
            if phy_name_N in nc_names and phy_name_P in nc_names:  # TBC, if need to compare values
                phy_model[i] = 1
            else:
                phy_model[i] = 0
    return nc_names, phy_names, num_phy, phy_model, \
           pth_names_alive, pth_names_dead, pth_names_attch, num_pth, pth_model


def construct_array_mmm(nc_in, simclass):
    nc_names = list(nc_in.variables.keys())

    # pathogens
    pth_names_alive = []
    pth_names_dead = []
    pth_names_attch = []
    for var in nc_names:
        if var.__contains__('WQ_PATH_') and var.__contains__('_ALIVE_CFU_M3'):
            pth_names_alive.append(var)
        if var.__contains__('WQ_PATH_') and var.__contains__('_DEAD_CFU_M3'):
            pth_names_dead.append(var)
    num_pth = np.size(pth_names_alive)
    pth_model = np.zeros([num_pth, 1], dtype=np.int32)
    for i in range(0, num_pth):
        pth_name_att = pth_names_alive[i].replace('_ALIVE_', '_ATTCHD_')
        if pth_name_att in nc_names:
            pth_model[i] = 1
            pth_names_attch.append(pth_name_att)
        else:
            pth_names_attch.append('')

    phy_names = []
    for var in nc_names:
        if var.__contains__('WQ_PHYTO_') and var.__contains__('_CONC_MM_M3'):
            phy_names.append(var)
    num_phy = np.size(phy_names)
    phy_model = np.zeros([num_phy, 1], dtype=np.int32)
    if simclass > 0:
        for i in range(0, num_phy):
            phy_name_N = phy_names[i].replace('_CONC_MM_M3', '_INT_N_MM_M3')
            phy_name_P = phy_names[i].replace('_CONC_MM_M3', '_INT_N_MM_M3')
            if phy_name_N in nc_names and phy_name_P in nc_names:  # TBC, if need to compare values
                phy_model[i] = 1
            else:
                phy_model[i] = 0
    return nc_names, phy_names, num_phy, phy_model, \
           pth_names_alive, pth_names_dead, pth_names_attch, num_pth, pth_model

def load_variables(nc_in, num_phy, phy_names,
                   num_pth, pth_names_alive, pth_names_dead, pth_names_attch, glb):
    oxy = nc_in['WQ_DISS_OXYGEN_MG_L'][:]
    [ncells, nsteps] = np.shape(oxy)
    WQvar = WQVariables(ncells, nsteps, num_phy, num_pth, glb.sim_class)
    WQvar.oxy = oxy
    simclass = glb.sim_class

    # pathogens
    for i in range(0, num_pth):
        WQvar.pth_ali[:, :, i] = nc_in[pth_names_alive[i]][:]
        WQvar.pth_dea[:, :, i] = nc_in[pth_names_dead[i]][:]
        try:
            WQvar.pth_att[:, :, i] = nc_in[pth_names_attch[i]][:]
        except:
            WQvar.pth_att[:, :, i] = 0.0

    if simclass > 0:
        WQvar.sil = nc_in['WQ_SILICATE_MG_L'][:]
        WQvar.amm = nc_in['WQ_AMMONIUM_MG_L'][:]
        WQvar.nit = nc_in['WQ_NITRATE_MG_L'][:]
        WQvar.frp = nc_in['WQ_FRP_MG_L'][:]
        try:
            WQvar.fra = nc_in['WQ_FRP_ADS_MG_L'][:]
        except:
            WQvar.fra = 0.0 * WQvar.frp
        for i in range(0, num_phy):
            WQvar.phy[:, :, i] = nc_in[phy_names[i]][:] * glb.X_cc[i] / glb.ug_per_mg
    if simclass > 1:
        WQvar.adding_new_attr('doc', nc_in['WQ_DOC_MG_L'][:])
        WQvar.adding_new_attr('don', nc_in['WQ_DON_MG_L'][:])
        WQvar.adding_new_attr('dop', nc_in['WQ_DOP_MG_L'][:])
        WQvar.adding_new_attr('poc', nc_in['WQ_POC_MG_L'][:])
        WQvar.adding_new_attr('pon', nc_in['WQ_PON_MG_L'][:])
        WQvar.adding_new_attr('pop', nc_in['WQ_POP_MG_L'][:])
    if simclass > 2:
        WQvar.adding_new_attr('rdc', nc_in['WQ_RDOC_MG_L'][:])
        WQvar.adding_new_attr('rdn', nc_in['WQ_RDON_MG_L'][:])
        WQvar.adding_new_attr('rdp', nc_in['WQ_RDOP_MG_L'][:])
        WQvar.adding_new_attr('rpc', nc_in['WQ_RPOM_MG_L'][:])
    return WQvar


def load_variables_mmm(nc_in, num_phy, phy_names,
                   num_pth, pth_names_alive, pth_names_dead, pth_names_attch, glb):
    L_per_m3 = glb.L_per_m3
    O2_mm_mg = glb.O2_mm_mg
    Si_mm_mg = glb.Si_mm_mg
    N_mm_mg = glb.N_mm_mg
    P_mm_mg = glb.P_mm_mg
    C_mm_mg = glb.C_mm_mg
    X_cc = glb.X_cc
    oxy = nc_in['WQ_DISS_OXYGEN_MM_M3'][:]/L_per_m3*O2_mm_mg
    [ncells, nsteps] = np.shape(oxy)
    WQvar = WQVariables(ncells, nsteps, num_phy, num_pth, glb.sim_class)
    WQvar.oxy = oxy
    simclass = glb.sim_class

    # pathogens
    for i in range(0, num_pth):
        WQvar.pth_ali[:, :, i] = nc_in[pth_names_alive[i]][:]/L_per_m3/10.0
        WQvar.pth_dea[:, :, i] = nc_in[pth_names_dead[i]][:]/L_per_m3/10.0
        try:
            WQvar.pth_att[:, :, i] = nc_in[pth_names_attch[i]][:]/L_per_m3/10.0
        except:
            WQvar.pth_att[:, :, i] = 0.0/L_per_m3/10.0

    if simclass > 0:
        WQvar.sil = nc_in['WQ_SILICATE_MM_M3'][:]/L_per_m3*Si_mm_mg
        WQvar.amm = nc_in['WQ_AMMONIUM_MM_M3'][:]/L_per_m3*N_mm_mg
        WQvar.nit = nc_in['WQ_NITRATE_MM_M3'][:]/L_per_m3*N_mm_mg
        WQvar.frp = nc_in['WQ_FRP_MM_M3'][:]/L_per_m3*P_mm_mg
        try:
            WQvar.fra = nc_in['WQ_FRP_ADS_MM_M3'][:]/L_per_m3*P_mm_mg
        except:
            WQvar.fra = 0.0 * WQvar.frp
        for i in range(0, num_phy):
            WQvar.phy[:, :, i] = nc_in[phy_names[i]][:] * X_cc[i] / glb.ug_per_mg *C_mm_mg/X_cc[i]
    if simclass > 1:
        WQvar.adding_new_attr('doc', nc_in['WQ_DOC_MM_M3'][:]/L_per_m3*C_mm_mg)
        WQvar.adding_new_attr('don', nc_in['WQ_DON_MM_M3'][:]/L_per_m3*N_mm_mg)
        WQvar.adding_new_attr('dop', nc_in['WQ_DOP_MM_M3'][:]/L_per_m3*P_mm_mg)
        WQvar.adding_new_attr('poc', nc_in['WQ_POC_MM_M3'][:]/L_per_m3*C_mm_mg)
        WQvar.adding_new_attr('pon', nc_in['WQ_PON_MM_M3'][:]/L_per_m3*N_mm_mg)
        WQvar.adding_new_attr('pop', nc_in['WQ_POP_MM_M3'][:]/L_per_m3*P_mm_mg)
    if simclass > 2:
        WQvar.adding_new_attr('rdc', nc_in['WQ_RDOC_MM_M3'][:]/L_per_m3*C_mm_mg)
        WQvar.adding_new_attr('rdn', nc_in['WQ_RDON_MM_M3'][:]/L_per_m3*N_mm_mg)
        WQvar.adding_new_attr('rdp', nc_in['WQ_RDOP_MM_M3'][:]/L_per_m3*P_mm_mg)
        WQvar.adding_new_attr('rpc', nc_in['WQ_RPOM_MM_M3'][:]/L_per_m3*C_mm_mg)
    return WQvar


def load_diagnostics(key, nc_in, num_phy, num_pth, pth_model, glb, var, NL, zfaces):
    [ncell, nstep] = np.shape(nc_in['WQ_DISS_OXYGEN_MG_L'][:])
    WQDiag = WQDiagnostics(ncell, nstep, num_phy, num_pth, glb.sim_class)
    simclass = glb.sim_class
    WQDiag.oxy_atm = nc_in['WQ_DIAG_O2_ATMOS_EXCHANGE_MG_M2_D'][:]
    WQDiag.oxy_sed = nc_in['WQ_DIAG_ACTUAL_O2_SED_FLUX_MG_M2_D'][:]
    dzs = WQMass.cell_dz(NL, zfaces)

    pat_tot_names, pat_grw_names, pat_lgt_names, pat_mor_names, \
    pat_als_names, pat_des_names, pat_ats_names, pat_atm_names = read_pat_field_names(key)
    att_count = 0
    for i in range(0, num_pth):
        WQDiag.pth_tot[:, :, i] = 1.0 * nc_in[pat_tot_names[i]][:]
        WQDiag.pth_grw[:, :, i] = 1.0 * nc_in[pat_grw_names[i]][:]
        WQDiag.pth_lgt_al[:, :, i] = 1.0 * nc_in[pat_lgt_names[i]][:]
        WQDiag.pth_lgt_de[:, :, i] = -1.0 * nc_in[pat_lgt_names[i]][:]
        WQDiag.pth_mor_al[:, :, i] = 1.0 * nc_in[pat_mor_names[i]][:]
        WQDiag.pth_mor_de[:, :, i] = -1.0 * nc_in[pat_mor_names[i]][:]
        WQDiag.pth_als[:, :, i] = 1.0 * nc_in[pat_als_names[i]][:] * 10.0 * glb.L_per_m3 * dzs
        WQDiag.pth_des[:, :, i] = 1.0 * nc_in[pat_des_names[i]][:] * 10.0 * glb.L_per_m3 * dzs
        if pth_model[i] != 0:
            WQDiag.pth_ats[:, :, i] = 1.0 * nc_in[pat_ats_names[att_count]][:] * 10.0 * glb.L_per_m3 * dzs
            WQDiag.pth_atm[:, :, i] = 1.0 * nc_in[pat_atm_names[att_count]][:]
            att_count = att_count + 1

    if simclass > 0:
        phc_sed_names, phn_sed_names, php_sed_names, phy_gpp_names, phy_res_names, phy_exc_names, phy_exn_names, \
        phy_exp_names, phy_moc_names, phy_mon_names, phy_mop_names = read_field_names(key)
        # atmosphere
        WQDiag.adding_new_attr('amm_atm',
                               nc_in['WQ_DIAG_DIN_ATMOS_EXCHANGE_MG_M2_D'][:] * (1.0 - glb.amm_nit_atm_split))
        WQDiag.adding_new_attr('nit_atm', nc_in['WQ_DIAG_DIN_ATMOS_EXCHANGE_MG_M2_D'][:] * glb.amm_nit_atm_split)
        WQDiag.adding_new_attr('frp_atm', nc_in['WQ_DIAG_DIP_ATMOS_EXCHANGE_MG_M2_D'][:])
        # sediment
        WQDiag.adding_new_attr('sil_sed', nc_in['WQ_DIAG_ACTUAL_SI_SED_FLUX_MG_M2_D'][:])
        WQDiag.adding_new_attr('amm_sed', nc_in['WQ_DIAG_ACTUAL_NH4_SED_FLUX_MG_M2_D'][:])
        WQDiag.adding_new_attr('nit_sed', nc_in['WQ_DIAG_ACTUAL_NO3_SED_FLUX_MG_M2_D'][:])
        WQDiag.adding_new_attr('frp_sed', nc_in['WQ_DIAG_ACTUAL_FRP_SED_FLUX_MG_M2_D'][:])
        # nitrification
        WQDiag.adding_new_attr('amm_nit', -1.0 * nc_in['WQ_DIAG_NITRIFICATION_MG_L_D'][:])
        WQDiag.oxy_nit = WQDiag.amm_nit * (1 / glb.N_mm_mg) * glb.O2_mm_mg * glb.Xon
        WQDiag.adding_new_attr('nit_nit', -1.0 * WQDiag.amm_nit)
        # Inorganic denitrification
        WQDiag.adding_new_attr('nit_den', -1.0 * nc_in['WQ_DIAG_DENITRIFICATION_MG_L_D'][:])
        # Inorganic anaerobic ammonium oxidation
        WQDiag.adding_new_attr('annamox', -1.0 * nc_in['WQ_DIAG_ANAER_NH4_OX_MG_L_D'][:])
        R = (glb.anammox_amm + glb.anammox_nit) * WQDiag.annamox / (
                glb.anammox_amm * var.amm + glb.anammox_nit * var.nit)
        amm_amx = R * glb.anammox_amm * var.amm / (glb.anammox_amm + glb.anammox_nit)
        nit_amx = R * glb.anammox_nit * var.nit / (glb.anammox_amm + glb.anammox_nit)
        WQDiag.adding_new_attr('amm_amx', amm_amx)
        WQDiag.adding_new_attr('nit_amx', nit_amx)
        # Inorganic dissim reduction of nitrate to ammonium
        WQDiag.adding_new_attr('amm_drn', 1.0 * nc_in['WQ_DIAG_DISS_NO3_RED_MG_L_D'][:])
        WQDiag.adding_new_attr('nit_drn', - nc_in['WQ_DIAG_DISS_NO3_RED_MG_L_D'][:])
        # Phytoplankton uptake
        # Nitrogen
        WQDiag.adding_new_attr('nit_upt', -1.0 * nc_in['WQ_DIAG_PHYTO_COM_NO3_UPTAKE_MG_L_D'][:])
        WQDiag.adding_new_attr('amm_upt', -1.0 * nc_in['WQ_DIAG_PHYTO_COM_NH4_UPTAKE_MG_L_D'][:])
        WQDiag.adding_new_attr('n2g_upt', 1.0 * nc_in['WQ_DIAG_PHYTO_COM_N2_UPTAKE_MG_L_D'][:])
        # Phosphorus
        WQDiag.adding_new_attr('frp_upt', -1.0 * nc_in['WQ_DIAG_PHYTO_COM_P_UPTAKE_MG_L_D'][:])
        # Phytoplankton sedimentation
        phc_sed = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        phn_sed = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        php_sed = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        # Phytoplankton uptake
        car_gpp = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        oxy_gpp = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        sil_upt = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        # Phytoplankton respiration
        car_rsf = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        oxy_rsf = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        # Phytoplankton excretion
        car_exc = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        amm_exc = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        frp_exc = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        sil_exc = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        # Phytoplankton mortality
        car_mor = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        amm_mor = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        frp_mor = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        sil_mor = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        for i in range(0, num_phy):
            # Phytoplankton sedimentation
            phc_sed[:, :, i] = 1.0 * nc_in[phc_sed_names[i]][:] * glb.L_per_m3 * glb.X_cc[i] * dzs / glb.ug_per_mg
            phn_sed[:, :, i] = 1.0 * nc_in[phn_sed_names[i]][:] * glb.L_per_m3 * dzs
            php_sed[:, :, i] = 1.0 * nc_in[php_sed_names[i]][:] * glb.L_per_m3 * dzs
            # Phytoplankton uptake
            car_gpp[:, :, i] = 1.0 * nc_in[phy_gpp_names[i]][:] * glb.X_cc[i] / glb.ug_per_mg
            oxy_gpp[:, :, i] = +1 * car_gpp[:, :, i] * (1 / glb.C_mm_mg) * glb.O2_mm_mg
            sil_upt[:, :, i] = -1 * car_gpp[:, :, i] * glb.X_scon[i] / glb.X_cc[i]
            # Phytoplankton respiration
            car_rsf[:, :, i] = 1.0 * nc_in[phy_res_names[i]][:] * glb.X_cc[i] / glb.ug_per_mg
            oxy_rsf[:, :, i] = +1 * car_rsf[:, :, i] * (1 / glb.C_mm_mg) * glb.O2_mm_mg
            # Phytoplankton excretion
            car_exc[:, :, i] = 1.0 * nc_in[phy_exc_names[i]][:] * glb.X_cc[i] / glb.ug_per_mg
            amm_exc[:, :, i] = - 1.0 * nc_in[phy_exn_names[i]][:]
            frp_exc[:, :, i] = - 1.0 * nc_in[phy_exp_names[i]][:]
            sil_exc[:, :, i] = ((-1 * car_rsf[:, :, i] / glb.k_fres[i] / var.phy[:, :, i]) * glb.K_fdom[0][i] *
                                (1.0 - 0.00) + (car_gpp[:, :, i] / var.phy[:, :, i]) * glb.f_pr[i]) \
                               * (glb.X_scon[i] / glb.X_cc[i]) * var.phy[:, :, i]
            # Phytoplankton mortality
            car_mor[:, :, i] = 1.0 * nc_in[phy_moc_names[i]][:] * glb.X_cc[i] / glb.ug_per_mg
            amm_mor[:, :, i] = - 1.0 * nc_in[phy_mon_names[i]][:]
            frp_mor[:, :, i] = - 1.0 * nc_in[phy_mop_names[i]][:]
            sil_mor[:, :, i] = -1 * (car_rsf[:, :, i] / glb.k_fres[i] / var.phy[:, :, i]) * \
                               (1.0 - glb.K_fdom[0][i]) * (glb.X_scon[i] / glb.X_cc[i]) * var.phy[:, :, i]
        WQDiag.adding_new_attr('phc_sed', phc_sed)
        WQDiag.adding_new_attr('phn_sed', phn_sed)
        WQDiag.adding_new_attr('php_sed', php_sed)
        WQDiag.car_gpp = car_gpp
        WQDiag.oxy_gpp = oxy_gpp
        WQDiag.sil_upt = sil_upt
        WQDiag.car_rsf = car_rsf
        WQDiag.oxy_rsf = oxy_rsf
        WQDiag.car_exc = car_exc
        WQDiag.amm_exc = amm_exc
        WQDiag.frp_exc = frp_exc
        WQDiag.sil_exc = sil_exc
        WQDiag.car_mor = car_mor
        WQDiag.amm_mor = amm_mor
        WQDiag.frp_mor = frp_mor
        WQDiag.sil_mor = sil_mor
    if simclass > 1:
        # Sediment
        WQDiag.adding_new_attr('doc_sed', nc_in['WQ_DIAG_ACTUAL_DOC_SED_FLUX_MG_M2_D'][:])
        WQDiag.adding_new_attr('don_sed', nc_in['WQ_DIAG_ACTUAL_DON_SED_FLUX_MG_M2_D'][:])
        WQDiag.adding_new_attr('dop_sed', nc_in['WQ_DIAG_ACTUAL_DOP_SED_FLUX_MG_M2_D'][:])
        WQDiag.adding_new_attr('doc_hyd', nc_in['WQ_DIAG_POC_HYDROL_MG_L_D'][:])
        WQDiag.adding_new_attr('don_hyd', nc_in['WQ_DIAG_PON_HYDROL_MG_L_D'][:])
        WQDiag.adding_new_attr('dop_hyd', nc_in['WQ_DIAG_POP_HYDROL_MG_L_D'][:])
        # Settling
        WQDiag.adding_new_attr('poc_sed', nc_in['WQ_DIAG_POC_SEDMTN_FLUX_MG_L_D'][:] * glb.L_per_m3 * dzs)
        WQDiag.adding_new_attr('pon_sed', nc_in['WQ_DIAG_PON_SEDMTN_FLUX_MG_L_D'][:] * glb.L_per_m3 * dzs)
        WQDiag.adding_new_attr('pop_sed', nc_in['WQ_DIAG_POP_SEDMTN_FLUX_MG_L_D'][:] * glb.L_per_m3 * dzs)
        # Hydrolysis
        WQDiag.adding_new_attr('poc_hyd', -nc_in['WQ_DIAG_POC_HYDROL_MG_L_D'][:])
        WQDiag.adding_new_attr('pon_hyd', -nc_in['WQ_DIAG_PON_HYDROL_MG_L_D'][:])
        WQDiag.adding_new_attr('pop_hyd', -nc_in['WQ_DIAG_POP_HYDROL_MG_L_D'][:])
        # Organic mineralisation organic sinks
        WQDiag.adding_new_attr('doc_min', -nc_in['WQ_DIAG_DOC_MINERL_MG_L_D'][:])
        WQDiag.adding_new_attr('don_min', -nc_in['WQ_DIAG_DON_MINERL_MG_L_D'][:])
        WQDiag.adding_new_attr('dop_min', -nc_in['WQ_DIAG_DOP_MINERL_MG_L_D'][:])
        # Excretion
        doc_exc = -1 * car_exc
        don_exc = amm_exc
        dop_exc = frp_exc
        WQDiag.adding_new_attr('doc_exc', doc_exc)
        WQDiag.adding_new_attr('don_exc', don_exc)
        WQDiag.adding_new_attr('dop_exc', dop_exc)
        amm_exc = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        frp_exc = np.zeros([ncell, nstep, num_phy], dtype=np.float32)  # clear up later
        WQDiag.amm_exc = amm_exc
        WQDiag.frp_exc = frp_exc
        # Mortality
        poc_mor = -1 * car_mor
        pon_mor = amm_mor
        pop_mor = frp_mor
        WQDiag.adding_new_attr('poc_mor', poc_mor)
        WQDiag.adding_new_attr('pon_mor', pon_mor)
        WQDiag.adding_new_attr('pop_mor', pop_mor)
        amm_mor = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        frp_mor = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        WQDiag.amm_mor = amm_mor
        WQDiag.frp_mor = frp_mor
        #  Organic mineralisation inorganic sinks
        oxy_omi = -1 * (nc_in['WQ_DIAG_DOC_MINERL_MG_L_D'][:] - nc_in['WQ_DIAG_DOC_MINERL_DENIT_MG_L_D'][:] -
                        nc_in['WQ_DIAG_DOC_MINERL_AN_MG_L_D'][:]) * (1 / glb.C_mm_mg) * glb.O2_mm_mg
        nit_omi = -1 * nc_in['WQ_DIAG_DOC_MINERL_DENIT_MG_L_D'][:] * (1 / glb.C_mm_mg) * glb.N_mm_mg
        WQDiag.oxy_omi = oxy_omi
        WQDiag.nit_omi = nit_omi
        # Organic mineralisation inorganic sources
        WQDiag.amm_omi = -WQDiag.don_min
        WQDiag.frp_omi = -WQDiag.dop_min
        # RPOM breakdown
        zeros = np.zeros([ncell, nstep], dtype=np.float32)
        WQDiag.adding_new_attr('poc_bdn', zeros)
        WQDiag.adding_new_attr('pon_bdn', zeros)
        WQDiag.adding_new_attr('pop_bdn', zeros)
        # Refractory activation
        WQDiag.adding_new_attr('doc_act', zeros)
        WQDiag.adding_new_attr('don_act', zeros)
        WQDiag.adding_new_attr('dop_act', zeros)
        # Refractory photolysis
        WQDiag.adding_new_attr('doc_pho', zeros)
        WQDiag.adding_new_attr('don_pho', zeros)
        WQDiag.adding_new_attr('dop_pho', zeros)
    if simclass > 2:
        # Settling
        WQDiag.adding_new_attr('rpc_sed', nc_in['WQ_DIAG_RPOM_SEDMTN_FLUX_MG_L_D'][:] * glb.L_per_m3 * dzs)
        # RPOM breakdown
        WQDiag.adding_new_attr('rpc_bdn', -nc_in['WQ_DIAG_RPOM_BREAKDOWN_MG_L_D'][:])
        WQDiag.adding_new_attr('poc_bdn', nc_in['WQ_DIAG_RPOM_BREAKDOWN_MG_L_D'][:])
        pon_bdn = -1 * WQDiag.rpc_bdn * glb.X_nrpom * (1 / glb.C_mm_mg) * glb.N_mm_mg
        pop_bdn = -1 * WQDiag.rpc_bdn * glb.X_prpom * (1 / glb.C_mm_mg) * glb.P_mm_mg
        WQDiag.adding_new_attr('pon_bdn', pon_bdn)
        WQDiag.adding_new_attr('pop_bdn', pop_bdn)
        # Refractory activation
        WQDiag.adding_new_attr('rdc_act', -nc_in['WQ_DIAG_RDOC_ACTV_MG_L_D'][:])
        WQDiag.adding_new_attr('rdn_act', -nc_in['WQ_DIAG_RDON_ACTV_MG_L_D'][:])
        WQDiag.adding_new_attr('rdp_act', -nc_in['WQ_DIAG_RDOP_ACTV_MG_L_D'][:])
        WQDiag.adding_new_attr('doc_act', -WQDiag.rdc_act)
        WQDiag.adding_new_attr('don_act', -WQDiag.rdn_act)
        WQDiag.adding_new_attr('dop_act', -WQDiag.rdp_act)
        # Photolysis
        try:
            rdc_pho = -1 * nc_in['WQ_DIAG_PHOTOLYSIS_MG_L_D'][:]
        except:
            rdc_pho = 0.0 * WQDiag.dop_act
        WQDiag.adding_new_attr('rdc_pho', rdc_pho)
        rdn_pho = WQDiag.rdc_pho * (var.rdn / var.rdc)
        rdp_pho = WQDiag.rdc_pho * (var.rdp / var.rdc)
        WQDiag.adding_new_attr('rdn_pho', rdn_pho)
        WQDiag.adding_new_attr('rdp_pho', rdp_pho)
        WQDiag.adding_new_attr('rdn_pho', rdn_pho)

        WQDiag.adding_new_attr('doc_pho', -1 * rdc_pho * glb.f_pho)
        WQDiag.adding_new_attr('don_pho', -1 * rdn_pho * glb.f_pho)
        WQDiag.adding_new_attr('dop_pho', -1 * rdp_pho * glb.f_pho)
        WQDiag.dic_pho = -1 * rdc_pho * (1.0 - glb.f_pho)
        WQDiag.amm_pho = -1 * rdn_pho * (1.0 - glb.f_pho)
        WQDiag.frp_pho = -1 * rdp_pho * (1.0 - glb.f_pho)

    return WQDiag


def load_diagnostics_mmm(key, nc_in, num_phy, num_pth, pth_model, glb, var, NL, zfaces):
    L_per_m3 = glb.L_per_m3
    O2_mm_mg = glb.O2_mm_mg
    Si_mm_mg = glb.Si_mm_mg
    N_mm_mg = glb.N_mm_mg
    P_mm_mg = glb.P_mm_mg
    C_mm_mg = glb.C_mm_mg
    X_cc = glb.X_cc
    [ncell, nstep] = np.shape(nc_in['WQ_DISS_OXYGEN_MM_M3'][:])
    WQDiag = WQDiagnostics(ncell, nstep, num_phy, num_pth, glb.sim_class)
    simclass = glb.sim_class
    WQDiag.oxy_atm = nc_in['WQ_DIAG_O2_ATMOS_EXCHANGE_MM_M2_D'][:]*O2_mm_mg
    WQDiag.oxy_sed = nc_in['WQ_DIAG_ACTUAL_O2_SED_FLUX_MM_M2_D'][:]*O2_mm_mg
    dzs = WQMass.cell_dz(NL, zfaces)

    pat_tot_names, pat_grw_names, pat_lgt_names, pat_mor_names, \
    pat_als_names, pat_des_names, pat_ats_names, pat_atm_names = read_pat_field_names_mmm(key)
    att_count = 0
    for i in range(0, num_pth):
        WQDiag.pth_tot[:, :, i] = 1.0 * nc_in[pat_tot_names[i]][:]/L_per_m3/10.0
        WQDiag.pth_grw[:, :, i] = 1.0 * nc_in[pat_grw_names[i]][:]/L_per_m3/10.0
        WQDiag.pth_lgt_al[:, :, i] = 1.0 * nc_in[pat_lgt_names[i]][:]/L_per_m3/10.0
        WQDiag.pth_lgt_de[:, :, i] = -1.0 * nc_in[pat_lgt_names[i]][:]/L_per_m3/10.0
        WQDiag.pth_mor_al[:, :, i] = 1.0 * nc_in[pat_mor_names[i]][:]/L_per_m3/10.0
        WQDiag.pth_mor_de[:, :, i] = -1.0 * nc_in[pat_mor_names[i]][:]/L_per_m3/10.0
        WQDiag.pth_als[:, :, i] = 1.0 * nc_in[pat_als_names[i]][:] * dzs
        WQDiag.pth_des[:, :, i] = 1.0 * nc_in[pat_des_names[i]][:] * dzs
        if pth_model[i] != 0:
            WQDiag.pth_ats[:, :, i] = 1.0 * nc_in[pat_ats_names[att_count]][:] * dzs
            WQDiag.pth_atm[:, :, i] = 1.0 * nc_in[pat_atm_names[att_count]][:]/L_per_m3/10.0
            att_count = att_count + 1
    if simclass > 0:
        phc_sed_names, phn_sed_names, php_sed_names, phy_gpp_names, phy_res_names, phy_exc_names, phy_exn_names, \
        phy_exp_names, phy_moc_names, phy_mon_names, phy_mop_names = read_field_names_MMM(key)
        # atmosphere
        WQDiag.adding_new_attr('amm_atm',
                               nc_in['WQ_DIAG_DIN_ATMOS_EXCHANGE_MM_M2_D'][:]*N_mm_mg * (1.0 - glb.amm_nit_atm_split))
        WQDiag.adding_new_attr('nit_atm', nc_in['WQ_DIAG_DIN_ATMOS_EXCHANGE_MM_M2_D'][:]*N_mm_mg * glb.amm_nit_atm_split)
        WQDiag.adding_new_attr('frp_atm', nc_in['WQ_DIAG_DIP_ATMOS_EXCHANGE_MM_M2_D'][:]*P_mm_mg)
        # sediment
        WQDiag.adding_new_attr('sil_sed', nc_in['WQ_DIAG_ACTUAL_SI_SED_FLUX_MM_M2_D'][:]*Si_mm_mg)
        WQDiag.adding_new_attr('amm_sed', nc_in['WQ_DIAG_ACTUAL_NH4_SED_FLUX_MM_M2_D'][:]*N_mm_mg)
        WQDiag.adding_new_attr('nit_sed', nc_in['WQ_DIAG_ACTUAL_NO3_SED_FLUX_MM_M2_D'][:]*N_mm_mg)
        WQDiag.adding_new_attr('frp_sed', nc_in['WQ_DIAG_ACTUAL_FRP_SED_FLUX_MM_M2_D'][:]*P_mm_mg)
        # nitrification
        WQDiag.adding_new_attr('amm_nit', -1.0 * nc_in['WQ_DIAG_NITRIFICATION_MM_M3_D'][:]/L_per_m3*N_mm_mg)
        WQDiag.oxy_nit = WQDiag.amm_nit * (1 / N_mm_mg) * O2_mm_mg * glb.Xon
        WQDiag.adding_new_attr('nit_nit', -1.0 * WQDiag.amm_nit)
        # Inorganic denitrification
        WQDiag.adding_new_attr('nit_den', -1.0 * nc_in['WQ_DIAG_DENITRIFICATION_MM_M3_D'][:]/L_per_m3*N_mm_mg)
        # Inorganic anaerobic ammonium oxidation
        WQDiag.adding_new_attr('annamox', -1.0 * nc_in['WQ_DIAG_ANAER_NH4_OX_MM_M3_D'][:]/L_per_m3*N_mm_mg)
        R = (glb.anammox_amm + glb.anammox_nit) * WQDiag.annamox / (
                glb.anammox_amm * var.amm + glb.anammox_nit * var.nit)
        amm_amx = R * glb.anammox_amm * var.amm / (glb.anammox_amm + glb.anammox_nit)
        nit_amx = R * glb.anammox_nit * var.nit / (glb.anammox_amm + glb.anammox_nit)
        WQDiag.adding_new_attr('amm_amx', amm_amx)
        WQDiag.adding_new_attr('nit_amx', nit_amx)
        # Inorganic dissim reduction of nitrate to ammonium
        WQDiag.adding_new_attr('amm_drn', 1.0 * nc_in['WQ_DIAG_DISS_NO3_RED_MM_M3_D'][:]/L_per_m3*N_mm_mg)
        WQDiag.adding_new_attr('nit_drn', - nc_in['WQ_DIAG_DISS_NO3_RED_MM_M3_D'][:]/L_per_m3*N_mm_mg)
        # Phytoplankton uptake
        # Nitrogen
        WQDiag.adding_new_attr('nit_upt', -1.0 * nc_in['WQ_DIAG_PHYTO_COM_NO3_UPTAKE_MM_M3_D'][:]/L_per_m3*N_mm_mg)
        WQDiag.adding_new_attr('amm_upt', -1.0 * nc_in['WQ_DIAG_PHYTO_COM_NH4_UPTAKE_MM_M3_D'][:]/L_per_m3*N_mm_mg)
        WQDiag.adding_new_attr('n2g_upt', 1.0 * nc_in['WQ_DIAG_PHYTO_COM_N2_UPTAKE_MM_M3_D'][:]/L_per_m3*N_mm_mg)
        # Phosphorus
        WQDiag.adding_new_attr('frp_upt', -1.0 * nc_in['WQ_DIAG_PHYTO_COM_P_UPTAKE_MM_M3_D'][:]/L_per_m3*P_mm_mg)
        # Phytoplankton sedimentation
        phc_sed = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        phn_sed = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        php_sed = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        # Phytoplankton uptake
        car_gpp = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        oxy_gpp = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        sil_upt = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        # Phytoplankton respiration
        car_rsf = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        oxy_rsf = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        # Phytoplankton excretion
        car_exc = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        amm_exc = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        frp_exc = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        sil_exc = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        # Phytoplankton mortality
        car_mor = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        amm_mor = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        frp_mor = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        sil_mor = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        for i in range(0, num_phy):
            # Phytoplankton sedimentation
            phc_sed[:, :, i] = 1.0 * nc_in[phc_sed_names[i]][:] * dzs / X_cc[i]*glb.ug_per_mg*X_cc[i]/glb.ug_per_mg*C_mm_mg
            phn_sed[:, :, i] = 1.0 * nc_in[phn_sed_names[i]][:] * dzs * N_mm_mg
            php_sed[:, :, i] = 1.0 * nc_in[php_sed_names[i]][:] * dzs * P_mm_mg
            # Phytoplankton uptake
            car_gpp[:, :, i] = +1.0 * nc_in[phy_gpp_names[i]][:] / glb.ug_per_mg * C_mm_mg
            oxy_gpp[:, :, i] = +1 * car_gpp[:, :, i] * (1 / C_mm_mg) * O2_mm_mg
            sil_upt[:, :, i] = -1 * car_gpp[:, :, i] * glb.X_scon[i] / X_cc[i]
            # Phytoplankton respiration
            car_rsf[:, :, i] = 1.0 * nc_in[phy_res_names[i]][:] * C_mm_mg / glb.ug_per_mg
            oxy_rsf[:, :, i] = +1 * car_rsf[:, :, i] * (1 / C_mm_mg) * O2_mm_mg
            # Phytoplankton excretion
            car_exc[:, :, i] = +1.0 * nc_in[phy_exc_names[i]][:] / glb.ug_per_mg * C_mm_mg
            amm_exc[:, :, i] = -1.0 * nc_in[phy_exn_names[i]][:] / L_per_m3 * N_mm_mg
            frp_exc[:, :, i] = -1.0 * nc_in[phy_exp_names[i]][:] / L_per_m3 * P_mm_mg
            sil_exc[:, :, i] = ((-1 * car_rsf[:, :, i] / glb.k_fres[i] / var.phy[:, :, i]) * glb.K_fdom[0][i] *
                                (1.0 - 0.00) + (car_gpp[:, :, i] / var.phy[:, :, i]) * glb.f_pr[i]) \
                               * (glb.X_scon[i] / glb.X_cc[i]) * var.phy[:, :, i]
            # Phytoplankton mortality
            car_mor[:, :, i] = +1.0 * nc_in[phy_moc_names[i]][:] * glb.X_cc[i] / glb.ug_per_mg / X_cc[i]*C_mm_mg
            amm_mor[:, :, i] = -1.0 * nc_in[phy_mon_names[i]][:] / L_per_m3 * N_mm_mg
            frp_mor[:, :, i] = -1.0 * nc_in[phy_mop_names[i]][:] / L_per_m3 * P_mm_mg
            sil_mor[:, :, i] = -1 * (car_rsf[:, :, i] / glb.k_fres[i] / var.phy[:, :, i]) * \
                               (1.0 - glb.K_fdom[0][i]) * (glb.X_scon[i] / glb.X_cc[i]) * var.phy[:, :, i]
        WQDiag.adding_new_attr('phc_sed', phc_sed)
        WQDiag.adding_new_attr('phn_sed', phn_sed)
        WQDiag.adding_new_attr('php_sed', php_sed)
        WQDiag.car_gpp = car_gpp
        WQDiag.oxy_gpp = oxy_gpp
        WQDiag.sil_upt = sil_upt
        WQDiag.car_rsf = car_rsf
        WQDiag.oxy_rsf = oxy_rsf
        WQDiag.car_exc = car_exc
        WQDiag.amm_exc = amm_exc
        WQDiag.frp_exc = frp_exc
        WQDiag.sil_exc = sil_exc
        WQDiag.car_mor = car_mor
        WQDiag.amm_mor = amm_mor
        WQDiag.frp_mor = frp_mor
        WQDiag.sil_mor = sil_mor
    if simclass > 1:
        # Sediment
        WQDiag.adding_new_attr('doc_sed', nc_in['WQ_DIAG_ACTUAL_DOC_SED_FLUX_MM_M2_D'][:]*C_mm_mg)
        WQDiag.adding_new_attr('don_sed', nc_in['WQ_DIAG_ACTUAL_DON_SED_FLUX_MM_M2_D'][:]*N_mm_mg)
        WQDiag.adding_new_attr('dop_sed', nc_in['WQ_DIAG_ACTUAL_DOP_SED_FLUX_MM_M2_D'][:]*P_mm_mg)
        WQDiag.adding_new_attr('doc_hyd', nc_in['WQ_DIAG_POC_HYDROL_MM_M3_D'][:]/L_per_m3*C_mm_mg)
        WQDiag.adding_new_attr('don_hyd', nc_in['WQ_DIAG_PON_HYDROL_MM_M3_D'][:]/L_per_m3*N_mm_mg)
        WQDiag.adding_new_attr('dop_hyd', nc_in['WQ_DIAG_POP_HYDROL_MM_M3_D'][:]/L_per_m3*P_mm_mg)
        # Settling
        WQDiag.adding_new_attr('poc_sed', nc_in['WQ_DIAG_POC_SEDMTN_FLUX_MM_M3_D'][:] * dzs * C_mm_mg)
        WQDiag.adding_new_attr('pon_sed', nc_in['WQ_DIAG_PON_SEDMTN_FLUX_MM_M3_D'][:] * dzs * N_mm_mg)
        WQDiag.adding_new_attr('pop_sed', nc_in['WQ_DIAG_POP_SEDMTN_FLUX_MM_M3_D'][:] * dzs * P_mm_mg)
        # Hydrolysis
        WQDiag.adding_new_attr('poc_hyd', -nc_in['WQ_DIAG_POC_HYDROL_MM_M3_D'][:]/L_per_m3*C_mm_mg)
        WQDiag.adding_new_attr('pon_hyd', -nc_in['WQ_DIAG_PON_HYDROL_MM_M3_D'][:]/L_per_m3*N_mm_mg)
        WQDiag.adding_new_attr('pop_hyd', -nc_in['WQ_DIAG_POP_HYDROL_MM_M3_D'][:]/L_per_m3*P_mm_mg)
        # Organic mineralisation organic sinks
        WQDiag.adding_new_attr('doc_min', -nc_in['WQ_DIAG_DOC_MINERL_MM_M3_D'][:]/L_per_m3*C_mm_mg)
        WQDiag.adding_new_attr('don_min', -nc_in['WQ_DIAG_DON_MINERL_MM_M3_D'][:]/L_per_m3*N_mm_mg)
        WQDiag.adding_new_attr('dop_min', -nc_in['WQ_DIAG_DOP_MINERL_MM_M3_D'][:]/L_per_m3*P_mm_mg)
        # Excretion
        doc_exc = -1 * car_exc
        don_exc = amm_exc
        dop_exc = frp_exc
        WQDiag.adding_new_attr('doc_exc', doc_exc)
        WQDiag.adding_new_attr('don_exc', don_exc)
        WQDiag.adding_new_attr('dop_exc', dop_exc)
        amm_exc = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        frp_exc = np.zeros([ncell, nstep, num_phy], dtype=np.float32)  # clear up later
        WQDiag.amm_exc = amm_exc
        WQDiag.frp_exc = frp_exc
        # Mortality
        poc_mor = -1 * car_mor
        pon_mor = amm_mor
        pop_mor = frp_mor
        WQDiag.adding_new_attr('poc_mor', poc_mor)
        WQDiag.adding_new_attr('pon_mor', pon_mor)
        WQDiag.adding_new_attr('pop_mor', pop_mor)
        amm_mor = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        frp_mor = np.zeros([ncell, nstep, num_phy], dtype=np.float32)
        WQDiag.amm_mor = amm_mor
        WQDiag.frp_mor = frp_mor
        #  Organic mineralisation inorganic sinks
        oxy_omi = -1 * (nc_in['WQ_DIAG_DOC_MINERL_MM_M3_D'][:]/L_per_m3*C_mm_mg - nc_in['WQ_DIAG_DOC_MINERL_DENIT_MM_M3_D'][:]/L_per_m3*C_mm_mg -
                        nc_in['WQ_DIAG_DOC_MINERL_AN_MM_M3_D'][:]/L_per_m3*C_mm_mg) * (1 / C_mm_mg) * O2_mm_mg
        nit_omi = -1 * nc_in['WQ_DIAG_DOC_MINERL_DENIT_MM_M3_D'][:]/L_per_m3*C_mm_mg * (1 / C_mm_mg) * N_mm_mg
        WQDiag.oxy_omi = oxy_omi
        WQDiag.nit_omi = nit_omi
        # Organic mineralisation inorganic sources
        WQDiag.amm_omi = -WQDiag.don_min
        WQDiag.frp_omi = -WQDiag.dop_min
        # RPOM breakdown
        zeros = np.zeros([ncell, nstep], dtype=np.float32)
        WQDiag.adding_new_attr('poc_bdn', zeros)
        WQDiag.adding_new_attr('pon_bdn', zeros)
        WQDiag.adding_new_attr('pop_bdn', zeros)
        # Refractory activation
        WQDiag.adding_new_attr('doc_act', zeros)
        WQDiag.adding_new_attr('don_act', zeros)
        WQDiag.adding_new_attr('dop_act', zeros)
        # Refractory photolysis
        WQDiag.adding_new_attr('doc_pho', zeros)
        WQDiag.adding_new_attr('don_pho', zeros)
        WQDiag.adding_new_attr('dop_pho', zeros)
    if simclass > 2:
        # Settling
        WQDiag.adding_new_attr('rpc_sed', nc_in['WQ_DIAG_RPOM_SEDMTN_FLUX_MM_M3_D'][:] * dzs * C_mm_mg)
        # RPOM breakdown
        WQDiag.adding_new_attr('rpc_bdn', -nc_in['WQ_DIAG_RPOM_BREAKDOWN_MM_M3_D'][:]/L_per_m3*C_mm_mg)
        WQDiag.adding_new_attr('poc_bdn', nc_in['WQ_DIAG_RPOM_BREAKDOWN_MM_M3_D'][:]/L_per_m3*C_mm_mg)
        pon_bdn = -1 * WQDiag.rpc_bdn * glb.X_nrpom * (1 / glb.C_mm_mg) * glb.N_mm_mg
        pop_bdn = -1 * WQDiag.rpc_bdn * glb.X_prpom * (1 / glb.C_mm_mg) * glb.P_mm_mg
        WQDiag.adding_new_attr('pon_bdn', pon_bdn)
        WQDiag.adding_new_attr('pop_bdn', pop_bdn)
        # Refractory activation
        WQDiag.adding_new_attr('rdc_act', -nc_in['WQ_DIAG_RDOC_ACTV_MM_M3_D'][:]/L_per_m3*C_mm_mg)
        WQDiag.adding_new_attr('rdn_act', -nc_in['WQ_DIAG_RDON_ACTV_MM_M3_D'][:]/L_per_m3*N_mm_mg)
        WQDiag.adding_new_attr('rdp_act', -nc_in['WQ_DIAG_RDOP_ACTV_MM_M3_D'][:]/L_per_m3*P_mm_mg)
        WQDiag.adding_new_attr('doc_act', -WQDiag.rdc_act)
        WQDiag.adding_new_attr('don_act', -WQDiag.rdn_act)
        WQDiag.adding_new_attr('dop_act', -WQDiag.rdp_act)
        # Photolysis
        try:
            rdc_pho = -1 * nc_in['WQ_DIAG_PHOTOLYSIS_MM_M3_D'][:]/L_per_m3*C_mm_mg
        except:
            rdc_pho = 0.0 * WQDiag.dop_act
        WQDiag.adding_new_attr('rdc_pho', rdc_pho)
        rdn_pho = WQDiag.rdc_pho * (var.rdn / var.rdc)
        rdp_pho = WQDiag.rdc_pho * (var.rdp / var.rdc)
        WQDiag.adding_new_attr('rdn_pho', rdn_pho)
        WQDiag.adding_new_attr('rdp_pho', rdp_pho)
        WQDiag.adding_new_attr('rdn_pho', rdn_pho)

        WQDiag.adding_new_attr('doc_pho', -1 * rdc_pho * glb.f_pho)
        WQDiag.adding_new_attr('don_pho', -1 * rdn_pho * glb.f_pho)
        WQDiag.adding_new_attr('dop_pho', -1 * rdp_pho * glb.f_pho)
        WQDiag.dic_pho = -1 * rdc_pho * (1.0 - glb.f_pho)
        WQDiag.amm_pho = -1 * rdn_pho * (1.0 - glb.f_pho)
        WQDiag.frp_pho = -1 * rdp_pho * (1.0 - glb.f_pho)

    return WQDiag

