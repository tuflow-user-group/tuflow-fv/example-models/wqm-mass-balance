function [pat_tot,pat_grw,pat_lgt,pat_mor, ...
            pat_asd,pat_dsd, ...
            pat_ats,pat_att] = pat_field_names_MMM(nc)
    
    pat_tot = nc(contains(nc,'WQ_DIAG_PATH') & ...
                             contains(nc,'_TOTAL_CFU_M3'));
    pat_grw = nc(contains(nc,'WQ_DIAG_PATH') & ...
                             contains(nc,'_GROWTH_CFU_M3_D'));
    pat_lgt = nc(contains(nc,'WQ_DIAG_PATH') & ...
                             contains(nc,'_LIGHT_CFU_M3_D'));
    pat_mor = nc(contains(nc,'WQ_DIAG_PATH') & ...
                             contains(nc,'_MORTALITY_CFU_M3_D'));
    pat_asd = nc(contains(nc,'WQ_DIAG_PATH') & ...
                             contains(nc,'_ALIVE_SEDMTN_CFU_M3_D'));
    pat_dsd = nc(contains(nc,'WQ_DIAG_PATH') & ...
                             contains(nc,'_DEAD_SEDMTN_CFU_M3_D'));
    pat_ats = nc(contains(nc,'WQ_DIAG_PATH') & ...
                             contains(nc,'_ATTCHD_SEDMTN_CFU_M3_D'));
    pat_att = nc(contains(nc,'WQ_DIAG_PATH') & ...
                             contains(nc,'_ATTACHMENT_CFU_M3_D'));















    
    
