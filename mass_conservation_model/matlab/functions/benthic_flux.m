function [sed] = benthic_flux(fl,NL,area)

[benth_cell_id] = sedi_ids(NL);
flux = sum(fl,3);

sed(1:size(flux,2)) = 0;
for k = 1:size(flux,2)
    for i = 1:length(benth_cell_id)
        sed(k) = sed(k) + sum(area(i)*flux(benth_cell_id(i),k));
    end
end

end


