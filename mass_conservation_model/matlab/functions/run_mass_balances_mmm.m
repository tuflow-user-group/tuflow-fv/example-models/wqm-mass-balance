
%% Rename variables
X_cc = X_cc_phy;
X_ncon = X_N_C_con_phy*14.*X_cc_phy/12; 
X_pcon = X_P_C_con_phy*31.*X_cc_phy/12;
X_scon = X_Si_C_con_phy*28.*X_cc_phy/12;
amm_nit_atm_split = f_TN_NO3;
k_fres = f_true_resp_phy; K_fdom = f_excr_loss_phy; f_pr = f_exud_phy;
X_nrpom = X_N_RPOM; X_prpom = X_P_RPOM;
f_pho = f_photo_RDOM;
run_IDs = runID;

%% Global inputs
model = 'WQM'; out_dir = [pwd,'\results\']; suffix = '_WQ.nc';
startdate = [2011,5,1,0,0,0]; tol = 0.1; % Plotting tolerance
save_all = 1; fig_count = 1; warning('off','all')

%% Execute
for i = 1:size(run_IDs,2)
    run_ID = run_IDs{i};
    if ~contains(run_ID,'MMM')
        disp('Keyword: MMM not found in run_ID, exiting')
        break
    end
    disp(['Executing mass balance for simulation: ',run_ID])
    mass_balance
end
