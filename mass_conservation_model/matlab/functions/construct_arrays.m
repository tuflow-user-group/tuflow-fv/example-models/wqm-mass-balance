[ncell,nstep] = size(netcdf_wq.WQ_DISS_OXYGEN_MG_L);
zero_flux = zeros(nstep,1);
zero_init = zeros(ncell,nstep,1);

% DO
% Preallocate arrays
oxy_nit = zero_init; oxy_gpp = zero_init;
oxy_rsf = zero_init; oxy_omi = zero_init;

% Number of pathogen groups
nc_names = netcdf_inq_varnames([out_dir,run_ID,'_WQ.nc']);
pth_names_alive = nc_names(contains(nc_names,'WQ_PATH_') & contains(nc_names,'_ALIVE_CFU_100ML'));
pth_names_dead = nc_names(contains(nc_names,'WQ_PATH_') & contains(nc_names,'_DEAD_CFU_100ML'));
num_pth = size(pth_names_alive,1); zero_init_pth = zeros(ncell,nstep,num_pth);
pth_model = ones(num_pth,1);
pth_ali = zero_init_pth; pth_dea = zero_init_pth; pth_att = zero_init_pth; 
pth_tot = zero_init_pth; pth_grw = zero_init_pth; pth_lgt = zero_init_pth;
pth_mor = zero_init_pth; pth_als = zero_init_pth; pth_des = zero_init_pth;
pth_ats = zero_init_pth; pth_atm = zero_init_pth;
% Determine basic or attached
for i = 1:num_pth
    k = strfind(pth_names_alive{i,:},'_ALIVE_CFU_100ML');
    expA = ['pth_att(:,:,i) = netcdf_wq.',pth_names_alive{i,1}(1:k),'ATTCHD_CFU_100ML;'];
    try
        eval(expA)
    catch exception
        pth_model(i) = 0;
    end
end

if sim_class > 0
    % Number of phyto groups
    nc_names = netcdf_inq_varnames([out_dir,run_ID,'_WQ.nc']);
    phy_names = nc_names(contains(nc_names,'WQ_PHYTO_') & contains(nc_names,'_CONC_MICG_L'));
    num_phy = size(phy_names,1); zero_init = zeros(ncell,nstep,num_phy);

    %Preallocate arrays
    oxy_nit = zero_init; oxy_gpp = zero_init;
    oxy_rsf = zero_init; oxy_omi = zero_init;
    phy_model = ones(num_phy,1);
    phy_IN = zero_init;   phy_IP = zero_init;
    phy = zero_init;     phy_sed = zero_init; 
    car_gpp = zero_init; car_rsf = zero_init;
    car_exc = zero_init; car_mor = zero_init;
    sil_upt = zero_init; sil_exc = zero_init; sil_mor = zero_init;
    amm_exc = zero_init; amm_mor = zero_init; amm_pho = zero_init; 
    frp_exc = zero_init; frp_mor = zero_init; frp_pho = zero_init;
    nit_omi = zero_init; amm_omi = zero_init; 
    frp_omi = zero_init; dic_pho = zero_init; 

    % Determine basic or advanced
    for i = 1:num_phy
        k = strfind(phy_names{i,:},'_CONC_MICG_L');
        expN = ['phy_IN(:,:,i) = netcdf_wq.',phy_names{i,1}(1:k),'INT_N_MG_L;'];
        expP = ['phy_IP(:,:,i) = netcdf_wq.',phy_names{i,1}(1:k),'INT_P_MG_L;'];    
        try
            eval(expN); eval(expP)
        catch exception
            phy_model(i) = 0;
        end
    end
end