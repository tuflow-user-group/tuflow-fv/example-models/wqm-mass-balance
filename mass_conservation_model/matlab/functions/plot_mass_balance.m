%close all

time = netcdf_wq.ResTime - netcdf_wq.ResTime(1);

figure(fig_count); clf; fig_count = fig_count + 1;
set(gcf,'pos',[10 298 816 590])
% DO
plot(time,oxy_mass_bal,'linewidth',lw); hold on
plot(time,pth_alive_attch_mass_bal,'k','linewidth',lw)
plot(time,pth_dead_mass_bal,'k:','linewidth',lw)
leg1 = legend('oxy','alive paths','dead paths');
title(run_ID, 'Interpreter', 'none')
set(leg1,'location','north','orientation','horizontal','NumColumns',1)
set(gca,'ylim',[-dy,dy]); grid on
xlabel('Simulation hour'); ylabel('Mass conservation error (%)')
 
% Inorganics
if sim_class > 0
    plot(time,sil_mass_bal,'--','linewidth',lw)
    plot(time,amm_mass_bal,'--','linewidth',lw); plot(time,nit_mass_bal,'--','linewidth',lw)
    plot(time,frp_mass_bal,'--','linewidth',lw)
    plot(time,phy_mass_bal,'--','linewidth',lw)
    if max(max(fra)==0)
        leg2 = legend('oxy','alive paths','dead paths', ...
        'sil','amm','nit','frp','phy');
        set(leg2,'location','north','orientation','horizontal','NumColumns',4)
    else
        leg2 = legend('oxy','alive paths','dead paths', ...
        'sil','amm','nit','frp+frpads','phy');
        set(leg2,'location','north','orientation','horizontal','NumColumns',4)
    end
end
  
% Labile organics
if sim_class > 1
    hold on
    plot(time,poc_mass_bal,':','linewidth',lw); plot(time,doc_mass_bal,':','linewidth',lw)
    plot(time,pon_mass_bal,':','linewidth',lw); plot(time,don_mass_bal,':','linewidth',lw)
    plot(time,dop_mass_bal,':','linewidth',lw); plot(time,dop_mass_bal,':','linewidth',lw)
    if max(max(fra)==0)
       leg3 = legend({strcat('oxy','alive paths','dead paths', ...
        'sil','amm','nit','frp','phy','poc','doc','pon','don','pop','dop')});
       set(leg3,'location','north','orientation','horizontal'); leg3.NumColumns = 4;
    else
       leg3 = legend({strcat('oxy','alive paths','dead paths', ...
        'sil','amm','nit','frp+frpads','phy','poc','doc','pon','don','pop','dop')});
       set(leg3,'location','north','orientation','horizontal'); leg3.NumColumns = 4;
    end
end
% refractory organics
if sim_class > 2
    hold on
    plot(time,rpc_mass_bal,':','linewidth',lw);
    plot(time,rdc_mass_bal,':','linewidth',lw); plot(time,rdn_mass_bal,':','linewidth',lw); plot(time,rdp_mass_bal,':','linewidth',lw);
    if max(max(fra)==0)
        leg4 = legend('oxy','alive paths','dead paths', ...
        'sil','amm','nit','frp','phy','poc','doc','pon','don','pop','dop','rpom','rdoc','rdon','rdop');
        set(leg4,'location','north','orientation','horizontal')
    else
        leg4 = legend('oxy','alive paths','dead paths', ...
        'sil','amm','nit','frp+frpads','phy','poc','doc','pon','don','pop','dop','rpom','rdoc','rdon','rdop');
        set(leg4,'location','north','orientation','horizontal')
    end
end

% Totals
figure(fig_count); clf; fig_count = fig_count + 1;
set(gcf,'pos',[836 298 816 590])
plot(time,tpt_mass_bal,':','linewidth',lw); hold on
leg5 = legend('Total path'); title(run_ID, 'Interpreter', 'none')
set(leg5,'location','north','orientation','horizontal')
set(gca,'ylim',[-dy,dy]); grid on
xlabel('Simulation hour'); ylabel('Mass conservation error (%)')
if sim_class > 0
    plot(time,tni_mass_bal,':','linewidth',lw)
    plot(time,tph_mass_bal,':','linewidth',lw)
    leg5 = legend('Total path','TN','TP'); title(run_ID, 'Interpreter', 'none')
    set(leg5,'location','north','orientation','horizontal')
    set(gca,'ylim',[-dy,dy]); grid on
    xlabel('Simulation hour'); ylabel('Mass conservation error (%)')
end