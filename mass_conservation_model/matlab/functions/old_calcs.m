%     % Losses
%     for i = 1:size(phy_res_names,1)
%         % Excretion
%         car_exc(:,:,i) = ((car_rsf(:,:,i) / k_fres(i) ./ phy(:,:,i)) * K_fdom(i) * (1.0 - k_fres(i)) + ...
%                          (-1 * car_gpp(:,:,i) ./ phy(:,:,i)) * f_pr(i)) * (X_cc(i)   / X_cc(i)) .* phy(:,:,i); 
%         if phy_model(i) == 0
%             amm_exc(:,:,i) = ((-1 * car_rsf(:,:,i) / k_fres(i) ./ phy(:,:,i)) * K_fdom(i) * (1.0 -   0.00) + ...
%                              (car_gpp(:,:,i) ./ phy(:,:,i)) * f_pr(i)) * (X_ncon(i) / X_cc(i)) .* phy(:,:,i); 
%             frp_exc(:,:,i) = ((-1 * car_rsf(:,:,i) / k_fres(i) ./ phy(:,:,i)) * K_fdom(i) * (1.0 -   0.00) + ...
%                              (car_gpp(:,:,i) ./ phy(:,:,i)) * f_pr(i)) * (X_pcon(i) / X_cc(i)) .* phy(:,:,i); 
%         else
%             amm_exc(:,:,i) = ((-1 * car_rsf(:,:,i) / k_fres(i) ./ phy(:,:,i)) * K_fdom(i) * (1.0 -   0.00) + ...
%                              (car_gpp(:,:,i) ./ phy(:,:,i)) * f_pr(i)) .* (phy_IN(:,:,i)); 
%             frp_exc(:,:,i) = ((-1 * car_rsf(:,:,i) / k_fres(i) ./ phy(:,:,i)) * K_fdom(i) * (1.0 -   0.00) + ...
%                              (car_gpp(:,:,i) ./ phy(:,:,i)) * f_pr(i)) .* (phy_IP(:,:,i)); 
%         end
%         sil_exc(:,:,i) = ((-1 * car_rsf(:,:,i) / k_fres(i) ./ phy(:,:,i)) * K_fdom(i) * (1.0 -   0.00) + ...
%                          (car_gpp(:,:,i) ./ phy(:,:,i)) * f_pr(i)) * (X_scon(i) / X_cc(i)) .* phy(:,:,i);
%         % Mortality
%         car_mor(:,:,i) = +1 * (car_rsf(:,:,i) / k_fres(i) ./ phy(:,:,i)) * (1.0 - K_fdom(i)) * ...
%                          (X_cc(i)   / X_cc(i)) .* phy(:,:,i) * (1.0 - k_fres(i)); 
%         if phy_model(i) == 0        
%             amm_mor(:,:,i) = -1 * (car_rsf(:,:,i) / k_fres(i) ./ phy(:,:,i)) * (1.0 - K_fdom(i)) * ...
%                              (X_ncon(i) / X_cc(i)) .* phy(:,:,i); 
%             frp_mor(:,:,i) = -1 * (car_rsf(:,:,i) / k_fres(i) ./ phy(:,:,i)) * (1.0 - K_fdom(i)) * ...
%                              (X_pcon(i) / X_cc(i)) .* phy(:,:,i); 
%         else
%             amm_mor(:,:,i) = -1 * (car_rsf(:,:,i) / k_fres(i) ./ phy(:,:,i)) * (1.0 - K_fdom(i)) .* ...
%                              (phy_IN(:,:,i)); 
%             frp_mor(:,:,i) = -1 * (car_rsf(:,:,i) / k_fres(i) ./ phy(:,:,i)) * (1.0 - K_fdom(i)) .* ...
%                              (phy_IP(:,:,i)); 
%         end
%         sil_mor(:,:,i) = -1 * (car_rsf(:,:,i) / k_fres(i) ./ phy(:,:,i)) * (1.0 - K_fdom(i)) * ...
%                          (X_scon(i) / X_cc(i)) .* phy(:,:,i);
%     end 
