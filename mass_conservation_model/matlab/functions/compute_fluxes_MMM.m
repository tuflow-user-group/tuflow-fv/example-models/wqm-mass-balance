% DO
[oxy_atm_flux] = (atmosph_flux(oxy_atm,NL,area) * conv_area)';
[oxy_sed_flux] = (benthic_flux(oxy_sed,NL,area) * conv_area)';
[oxy_mass] = (mass(oxy,NL,zfaces,area) * conv_mass)';
[oxy_nitrif_flux] = (vol_flux(oxy_nit,NL,zfaces,area) * conv_volu)';
[oxy_phy_gpp_flux] = (vol_flux(oxy_gpp,NL,zfaces,area) * conv_volu)';
[oxy_phy_res_flux] = (vol_flux(oxy_rsf,NL,zfaces,area) * conv_volu)';
[oxy_org_min_flux] = (vol_flux(oxy_omi,NL,zfaces,area) * conv_volu)';

% Pathogens
[pat_ali_mass] = (mass(pth_ali,NL,zfaces,area) * conv_mass_CFU)';
[pat_dea_mass] = (mass(pth_dea,NL,zfaces,area) * conv_mass_CFU)';
[pat_att_mass] = (mass(pth_att,NL,zfaces,area) * conv_mass_CFU)';
pat_alat_mass = pat_ali_mass + pat_att_mass;  
[pat_grw_flux] = (vol_flux(pth_grw,NL,zfaces,area) * conv_volu_CFU)';  
[pat_alat_lgt_flux] = (vol_flux(pth_lgt_al,NL,zfaces,area) * conv_volu_CFU)';  
[pat_alat_mor_flux] = (vol_flux(pth_mor_al,NL,zfaces,area) * conv_volu_CFU)';  
[pat_dead_lgt_flux] = (vol_flux(pth_lgt_de,NL,zfaces,area) * conv_volu_CFU)';  
[pat_dead_mor_flux] = (vol_flux(pth_mor_de,NL,zfaces,area) * conv_volu_CFU)';  
[pat_atm_flux] = (vol_flux(pth_atm,NL,zfaces,area) * conv_volu_CFU)';  
[pat_alat_al_sed_flux] = (benthic_flux(pth_als,NL,area) * conv_area_CFU)';
[pat_dead_de_sed_flux] = (benthic_flux(pth_des,NL,area) * conv_area_CFU)';
[pat_alat_at_sed_flux] = (benthic_flux(pth_ats,NL,area) * conv_area_CFU)';

% Inorganics
if sim_class > 0   
    [sil_sed_flux] = (benthic_flux(sil_sed,NL,area) * conv_area)';
    [sil_mass] = (mass(sil,NL,zfaces,area) * conv_mass)';
    [sil_phy_upt_flux] = (vol_flux(sil_upt,NL,zfaces,area) * conv_volu)';   
    [sil_phy_mor_flux] = (vol_flux(sil_mor,NL,zfaces,area) * conv_volu)';
    [sil_phy_exc_flux] = (vol_flux(sil_exc,NL,zfaces,area) * conv_volu)';
    
    [amm_atm_flux] = (atmosph_flux(amm_atm,NL,area) * conv_area)';
    [amm_sed_flux] = (benthic_flux(amm_sed,NL,area) * conv_area)';
    [amm_mass] = (mass(amm,NL,zfaces,area) * conv_mass)';
    [amm_nitrif_flux]  = (vol_flux(amm_nit,NL,zfaces,area) * conv_volu)';
    [amm_anmmox_flux]  = (vol_flux(amm_amx,NL,zfaces,area) * conv_volu)';
    [amm_drna_flux]    = (vol_flux(amm_drn,NL,zfaces,area) * conv_volu)';
    [amm_phy_upt_flux] = (vol_flux(amm_upt,NL,zfaces,area) * conv_volu)';   
    [amm_phy_mor_flux] = (vol_flux(amm_mor,NL,zfaces,area) * conv_volu)';
    [amm_phy_exc_flux] = (vol_flux(amm_exc,NL,zfaces,area) * conv_volu)';
    [amm_org_min_flux] = (vol_flux(amm_omi,NL,zfaces,area) * conv_volu)';
    [amm_org_pho_flux] = (vol_flux(amm_pho,NL,zfaces,area) * conv_volu)';
    
    [nit_atm_flux] = (atmosph_flux(nit_atm,NL,area) * conv_area)';
    [nit_sed_flux] = (benthic_flux(nit_sed,NL,area) * conv_area)';
    [nit_mass] = (mass(nit,NL,zfaces,area) * conv_mass)';
    [nit_nitrif_flux]  = (vol_flux(nit_nit,NL,zfaces,area) * conv_volu)';
    [nit_anmmox_flux]  = (vol_flux(nit_amx,NL,zfaces,area) * conv_volu)';
    [nit_drna_flux]    = (vol_flux(nit_drn,NL,zfaces,area) * conv_volu)';
    [nit_denit_flux]   = (vol_flux(nit_den,NL,zfaces,area) * conv_volu)';   
    [nit_phy_upt_flux] = (vol_flux(nit_upt,NL,zfaces,area) * conv_volu)';   
    [nit_org_min_flux] = (vol_flux(nit_omi,NL,zfaces,area) * conv_volu)';
    [nit_phy_fix_flux] = (vol_flux(n2g_upt,NL,zfaces,area) * conv_volu)';

    [frp_atm_flux] = (atmosph_flux(frp_atm,NL,area) * conv_area)';
    [frp_sed_flux] = (benthic_flux(frp_sed,NL,area) * conv_area)';
    [frp_mass] = (mass(frp+fra,NL,zfaces,area) * conv_mass)';
    [frp_phy_upt_flux] = (vol_flux(frp_upt,NL,zfaces,area) * conv_volu)';   
    [frp_phy_mor_flux] = (vol_flux(frp_mor,NL,zfaces,area) * conv_volu)';
    [frp_phy_exc_flux] = (vol_flux(frp_exc,NL,zfaces,area) * conv_volu)';
    [frp_org_min_flux] = (vol_flux(frp_omi,NL,zfaces,area) * conv_volu)';
    [frp_org_pho_flux] = (vol_flux(frp_pho,NL,zfaces,area) * conv_volu)';

    [phy_mass] = (mass(phy,NL,zfaces,area) * conv_mass)';
    [phy_phy_sed_flux] = (benthic_flux(phc_sed,NL,area) * conv_area)';
    [ntn_phy_sed_flux] = (benthic_flux(phn_sed,NL,area) * conv_area)';
    [phs_phy_sed_flux] = (benthic_flux(php_sed,NL,area) * conv_area)';
    [phy_phy_upt_flux] = (vol_flux(car_gpp,NL,zfaces,area) * conv_volu)';  
    [phy_phy_res_flux] = (vol_flux(car_rsf,NL,zfaces,area) * conv_volu)';
    [phy_phy_mor_flux] = (vol_flux(car_mor,NL,zfaces,area) * conv_volu)';
    [phy_phy_exc_flux] = (vol_flux(car_exc,NL,zfaces,area) * conv_volu)';
    
    [doc_sed_flux] = zero_flux; [don_sed_flux] = zero_flux; [dop_sed_flux] = zero_flux;
    [poc_bdn_flux] = zero_flux; [pon_bdn_flux] = zero_flux; [pop_bdn_flux] = zero_flux;
    [poc_sed_flux] = zero_flux; [pon_sed_flux] = zero_flux; [pop_sed_flux] = zero_flux;
end

% Labile organics
if sim_class > 1
    [doc_sed_flux] = (benthic_flux(doc_sed,NL,area) * conv_area)';
    [doc_mass] = (mass(doc,NL,zfaces,area) * conv_mass)';
    [doc_phy_exc_flux] = (vol_flux(doc_exc,NL,zfaces,area) * conv_volu)';
    [doc_hyd_flux] = (vol_flux(doc_hyd,NL,zfaces,area) * conv_volu)';
    [doc_min_flux] = (vol_flux(doc_min,NL,zfaces,area) * conv_volu)';
    [doc_act_flux] = (vol_flux(doc_act,NL,zfaces,area) * conv_volu)';
    [doc_pho_flux] = (vol_flux(doc_pho,NL,zfaces,area) * conv_volu)';
   
    [poc_phy_mor_flux] = (vol_flux(poc_mor,NL,zfaces,area) * conv_volu)';
    [poc_mass] = (mass(poc,NL,zfaces,area) * conv_mass)';
    [poc_hyd_flux] = (vol_flux(poc_hyd,NL,zfaces,area) * conv_volu)';
    [poc_bdn_flux] = (vol_flux(poc_bdn,NL,zfaces,area) * conv_volu)';
    [poc_sed_flux] = (benthic_flux(poc_sed,NL,area) * conv_area)';
    
    [don_sed_flux] = (benthic_flux(don_sed,NL,area) * conv_area)';
    [don_mass] = (mass(don,NL,zfaces,area) * conv_mass)';
    [don_phy_exc_flux] = (vol_flux(don_exc,NL,zfaces,area) * conv_volu)';
    [don_hyd_flux] = (vol_flux(don_hyd,NL,zfaces,area) * conv_volu)';
    [don_min_flux] = (vol_flux(don_min,NL,zfaces,area) * conv_volu)';
    [don_act_flux] = (vol_flux(don_act,NL,zfaces,area) * conv_volu)';
    [don_pho_flux] = (vol_flux(don_pho,NL,zfaces,area) * conv_volu)';
    
    [pon_phy_mor_flux] = (vol_flux(pon_mor,NL,zfaces,area) * conv_volu)';
    [pon_mass] = (mass(pon,NL,zfaces,area) * conv_mass)';
    [pon_hyd_flux] = (vol_flux(pon_hyd,NL,zfaces,area) * conv_volu)';
    [pon_bdn_flux] = (vol_flux(pon_bdn,NL,zfaces,area) * conv_volu)';
    [pon_sed_flux] = (benthic_flux(pon_sed,NL,area) * conv_area)';

    [dop_sed_flux] = (benthic_flux(dop_sed,NL,area) * conv_area)';
    [dop_mass] = (mass(dop,NL,zfaces,area) * conv_mass)';
    [dop_phy_exc_flux] = (vol_flux(dop_exc,NL,zfaces,area) * conv_volu)';
    [dop_hyd_flux] = (vol_flux(dop_hyd,NL,zfaces,area) * conv_volu)';
    [dop_min_flux] = (vol_flux(dop_min,NL,zfaces,area) * conv_volu)';
    [dop_act_flux] = (vol_flux(dop_act,NL,zfaces,area) * conv_volu)';
    [dop_pho_flux] = (vol_flux(dop_pho,NL,zfaces,area) * conv_volu)';

    [pop_phy_mor_flux] = (vol_flux(pop_mor,NL,zfaces,area) * conv_volu)';
    [pop_mass] = (mass(pop,NL,zfaces,area) * conv_mass)';
    [pop_hyd_flux] = (vol_flux(pop_hyd,NL,zfaces,area) * conv_volu)';
    [pop_bdn_flux] = (vol_flux(pop_bdn,NL,zfaces,area) * conv_volu)';
    [pop_sed_flux] = (benthic_flux(pop_sed,NL,area) * conv_area)';
end

% Refractory organics
if sim_class > 2
    [rdc_mass] = (mass(rdc,NL,zfaces,area) * conv_mass)';
    [rdc_act_flux] = (vol_flux(rdc_act,NL,zfaces,area) * conv_volu)';
    [rdc_pho_flux] = (vol_flux(rdc_pho,NL,zfaces,area) * conv_volu)';
    
    [rdn_mass] = (mass(rdn,NL,zfaces,area) * conv_mass)';
    [rdn_act_flux] = (vol_flux(rdn_act,NL,zfaces,area) * conv_volu)';
    [rdn_pho_flux] = (vol_flux(rdn_pho,NL,zfaces,area) * conv_volu)';

    [rdp_mass] = (mass(rdp,NL,zfaces,area) * conv_mass)';
    [rdp_act_flux] = (vol_flux(rdp_act,NL,zfaces,area) * conv_volu)';
    [rdp_pho_flux] = (vol_flux(rdp_pho,NL,zfaces,area) * conv_volu)';

    [rpc_mass] = (mass(rpc,NL,zfaces,area) * conv_mass)';
    [rpc_sed_flux] = (benthic_flux(rpc_sed,NL,area) * conv_area)';    
    [rpc_bdn_flux] = (vol_flux(rpc_bdn,NL,zfaces,area) * conv_volu)';
end

% Totals 
% Pathogens
[tpt_mass] = (mass(pth_tot,NL,zfaces,area) * conv_mass_CFU)' - ...;
                 cumsum(pat_alat_al_sed_flux + pat_dead_de_sed_flux + pat_alat_at_sed_flux);
if sim_class > 0
    [tni_mass] = (mass(netcdf_wq.WQ_DIAG_TOTAL_NITROGEN_MM_M3/L_per_m3*N_mm_mg,NL,zfaces,area) * conv_mass)' - ...
                 cumsum(amm_sed_flux + amm_atm_flux + ...
                        nit_sed_flux + nit_atm_flux + nit_denit_flux + ...
                        amm_anmmox_flux + nit_anmmox_flux + ...
                        nit_phy_fix_flux + ...
                        ntn_phy_sed_flux + ...
                        nit_org_min_flux + ...
                        don_sed_flux + ...
                        pon_bdn_flux + ...
                        pon_sed_flux);                    
    [tkn_mass] = (mass(netcdf_wq.WQ_DIAG_TOTAL_KJELDAHL_NITROGEN_MM_M3/L_per_m3*N_mm_mg,NL,zfaces,area) * conv_mass)' - ...
                 cumsum(amm_sed_flux + amm_atm_flux + amm_nitrif_flux + ...
                        amm_anmmox_flux + ...
                        ntn_phy_sed_flux + ...
                        don_sed_flux + ...
                        pon_bdn_flux + ...
                        pon_sed_flux);
    [tph_mass] = (mass(netcdf_wq.WQ_DIAG_TOTAL_PHOSPHORUS_MM_M3/L_per_m3*P_mm_mg,NL,zfaces,area) * conv_mass)' - ...
                 cumsum(frp_sed_flux + frp_atm_flux + ...
                        phs_phy_sed_flux + ...
                        dop_sed_flux + ...
                        pop_bdn_flux + ...
                        pop_sed_flux);
    [toc_mass] = (mass(netcdf_wq.WQ_DIAG_TOTAL_ORGANIC_CARBON_MM_M3/L_per_m3*C_mm_mg,NL,zfaces,area) * conv_mass)';
end    
