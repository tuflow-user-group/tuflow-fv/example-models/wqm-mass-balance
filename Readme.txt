Uploaded 30-01-2023 

See water quality user manual for description and instructions:
https://wqm-manual.tuflow.com/AppMBal.html

===PLEASE NOTE=======================================================================================

(1) Ensure that the scripts in the ./matlab/functions directory (i.e. those downloaded)
are being called when the mass balance scripts are executed, and not deprecated versions
that may be on your computer/network and reside in folders that remain in your Matlab path. These
deprecated scripts may not work because a number of upgrades have been made
since the previous release. 

To ensure that older deprecated scripts are not being called:
-  Remove all references in your Matlab path to the folders that contain deprecated scripts.
This can be done either manually or by using the interactive Matlab path dialogue boxes
-  Navigate your Matlab working directory to be the top directory of the location to which the model
 has been downloaded. This can be done either with the 'cd' command at the Matlab command
 prompt or through the interactive Matlab folder navigator
-  Once in this top level directory, go to the Matlab command prompt and type:
     addpath(genpath(pwd),'-begin');
This will add the entire folder structure of the latest model download to your Matlab path, 
and put these folders at the top of your Matlab path variable. This means that Matlab will look at these
folders for lower level scripts before it looks anywhere else

(3) All scripts have been tested using Matlab Release 2022a and later. Use of older Matlab releases should
be avoided. Some undesirable behaviours are known to occur in these earlier releases.
=====================================================================================================

Happy modelling from the TUFLOW Team. 
If you have any questions or would like to contribute please get in touch via support@tuflow.com.
See also:
-  https://wqm-manual.tuflow.com/AppMBal.html

